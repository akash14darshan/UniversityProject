﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public static class TcpSender
{
    const int PORT_NO = 9000;
    const string SERVER_IP = "163.172.255.37";

    public static async Task<byte[]> SendMessage(byte[] dataToSend)
    {
        return await Task.Run(() => 
        {
            try
            {
                TcpClient client = new TcpClient(SERVER_IP, PORT_NO);
                NetworkStream nwStream = client.GetStream();
                nwStream.Write(dataToSend, 0, dataToSend.Length);

                byte[] bytesToRead = new byte[client.ReceiveBufferSize];
                int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                return bytesToRead;

            }
            catch (Exception)
            {
                return null;
            }
        });
    }
}
