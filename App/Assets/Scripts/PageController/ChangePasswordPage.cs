﻿using ProjectServer.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

class ChangePasswordPage : MonoBehaviour
{
    [SerializeField] InputField Password;
    [SerializeField] InputField Password1;
    [SerializeField] Text Status;
    [SerializeField] GameObject Device_Page;
    [SerializeField] Button Back;
    [SerializeField] Button ChangePassword;

    private void Awake()
    {
        Back.onClick.AddListener(() => { gameObject.SetActive(false); Device_Page.SetActive(true); });
        ChangePassword.onClick.AddListener(() => { OnChangePassword(); });
        Password.onValueChanged.AddListener((s) => Clear());
        Password1.onValueChanged.AddListener((s) => Clear());
    }

    private void Clear()
    {
        Status.text = null;
    }

    private void OnDisable()
    {
        Clear();
        Password.text = null;
        Password1.text = null;
    }

    async void OnChangePassword()
    {
        Back.interactable = false;
        ChangePassword.interactable = false;

        string email = Login.SuccessEmail;
        string password = Password.text;
        string password1 = Password1.text;

        if(password == password1)
        {
            if(password.Length >= 8)
            {
                if(Login.CheckValidEmail(email))
                {
                    using(MemoryStream stream = new MemoryStream())
                    {
                        ByteProxy.Serialize(stream, 3);
                        StringProxy.Serialize(stream, email);
                        StringProxy.Serialize(stream, password);

                        byte[] retData = await TcpSender.SendMessage(stream.ToArray());

                        if(retData != null && retData.Length > 0)
                        {
                            switch(retData[0])
                            {
                                case 1:
                                    Status.text = "Changed Password Successfully";
                                    Status.color = Color.black;
                                    break;
                                case 2:
                                    Status.text = "Unexpected Error";
                                    Status.color = Color.red;
                                    break;
                                case 3:
                                    Status.text = "New password same as old";
                                    Status.color = Color.red;
                                    break;
                                case 254:
                                    Status.text = "Invalid Auth ID";
                                    Status.color = Color.red;
                                    break;
                                case 255:
                                    Status.text = "Internal Server Error";
                                    Status.color = Color.red;
                                    break;
                            }
                        }
                        else
                        {
                            Status.text = "No Internet";
                            Status.color = Color.red;
                        }
                    }
                }
                else
                {
                    Status.text = "Login expired.\n Re login and try again";
                    Status.color = Color.red;
                }
            }
            else
            {
                Status.text = "Password length should be more than 8";
                Status.color = Color.red;
            }
        }
        else
        {
            Status.text = "Password not matching";
            Status.color = Color.red;
        }

        Back.interactable = true;
        ChangePassword.interactable = true;
    }
}
