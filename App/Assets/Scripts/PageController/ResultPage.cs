﻿using UnityEngine;
using UnityEngine.UI;

class ResultPage : MonoBehaviour
{
    [SerializeField] Button Back;
    [SerializeField] GameObject DeviceID_Page;

    private void Awake()
    {
        Back.onClick.AddListener(() => { gameObject.SetActive(false); DeviceID_Page.SetActive(true); });
    }
}
