﻿using ProjectServer.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    [SerializeField] Button LoginButton;
    [SerializeField] Button RegisterButton;
    [SerializeField] Button QuitButton;

    [SerializeField] InputField Email;
    [SerializeField] InputField Password;

    [SerializeField] GameObject DeviceID_Page;
    [SerializeField] GameObject Registration_Page;

    [SerializeField] Text Status;

    private void Awake()
    {
        LoginButton.onClick.AddListener(() => { OnLogin(); });
        RegisterButton.onClick.AddListener(() => { gameObject.SetActive(false); Registration_Page.SetActive(true); });
        QuitButton.onClick.AddListener(() =>
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        });

        Email.onValueChanged.AddListener((s) => ClearStatus());
        Password.onValueChanged.AddListener((s) => ClearStatus());
    }

    private void OnDisable()
    {
        Status.text = null;
        Email.text = null;
        Password.text = null;
    }

    void ClearStatus()
    {
        Status.text = null;
    }

    private async void OnLogin()
    {
        ClearStatus();
        LoginButton.interactable = false;
        RegisterButton.interactable = false;

        string email = Email.text;
        string password = Password.text;

        if(CheckValidEmail(email))
        {
            if (!string.IsNullOrEmpty(password))
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    ByteProxy.Serialize(stream, 1);
                    StringProxy.Serialize(stream, email);
                    StringProxy.Serialize(stream, password);
                    byte[] returnData = await TcpSender.SendMessage(stream.ToArray());

                    if(returnData != null && returnData.Length > 0)
                    {
                        switch(returnData[0])
                        {
                            case 1:
                                OnLoginSuccess(email, password);
                                break;
                            case 2:
                                Status.text = "Incorrect Password";
                                Status.color = Color.red;
                                break;
                            case 3:
                                Status.text = "Invalid Email";
                                Status.color = Color.red;
                                break;
                            case 254:
                                Status.text = "Invalid Auth ID";
                                Status.color = Color.red;
                                break;
                            case 255:
                                Status.text = "Internal Server Error";
                                Status.color = Color.red;
                                break;
                        }
                    }
                    else
                    {
                        Status.text = "No Internet";
                        Status.color = Color.red;
                    }
                }
            }
            else
            {
                Status.text = "Invalid Password";
                Status.color = Color.red;
            }
        }

        else
        {
            Status.text = "Invalid Email";
            Status.color = Color.red;
        }

        LoginButton.interactable = true;
        RegisterButton.interactable = true;
    }

    private void OnEnable()
    {
        LoginButton.interactable = true;
        SuccessEmail = null;
        SuccessPassword = null;
    }

    private void OnLoginSuccess(string email, string password)
    {
        if(gameObject.activeSelf)
        {
            SuccessEmail = email;
            SuccessPassword = password;
            gameObject.SetActive(false);
            DeviceID_Page.SetActive(true);
        }
    }

    public static bool CheckValidEmail(string email)
    {
        return !string.IsNullOrEmpty(email) && email.Contains("@") && email.Contains(".");
    }

    public static string SuccessEmail;
    public static string SuccessPassword;
}
