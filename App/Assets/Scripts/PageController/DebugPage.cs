﻿using System;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class DebugPage : MonoBehaviour
{
    [SerializeField] InputField Field;
    [SerializeField] Button Send;
    [SerializeField] Text Message;

    const int PORT_NO = 9000;
    const string SERVER_IP = "163.172.255.37";

    private void Awake()
    {
        Send.onClick.AddListener(SendMessage);
    }

    private void SendMessage()
    {
        Field.interactable = false;
        try
        {
            string textToSend = Field.text;

            if (string.IsNullOrEmpty(textToSend)) return;
            TcpClient client = new TcpClient(SERVER_IP, PORT_NO);
            NetworkStream nwStream = client.GetStream();
            byte[] dataToSend = Encoding.UTF8.GetBytes(textToSend);
            nwStream.Write(dataToSend, 0, dataToSend.Length);

            byte[] bytesToRead = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
            string textReply = Encoding.UTF8.GetString(bytesToRead, 0, bytesRead);
            Message.text = $"Received reply at {DateTime.Now}\n\n" + textReply;
        }
        catch(Exception e)
        {
            Message.text = $"Error occured with socket:\n\n" + e.ToString();
        }
        Field.interactable = true;
    }
}
