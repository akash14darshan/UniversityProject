﻿using UnityEngine;

class PageInit : MonoBehaviour
{
    private void Awake()
    {
        foreach(Transform transform in transform)
        {
            if (transform.GetComponent<Login>() != null) transform.gameObject.SetActive(true);
            else transform.gameObject.SetActive(false);
        }
    }
}
