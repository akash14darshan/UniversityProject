﻿using ProjectServer.Serialization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

class RegistrationPage : MonoBehaviour
{
    [SerializeField] Text Status;
    [SerializeField] Button RegisterButton;
    [SerializeField] Button Back;
    [SerializeField] GameObject LoginPage;

    [SerializeField] InputField Email;
    [SerializeField] InputField Password;
    [SerializeField] InputField Password1;

    private void OnDisable()
    {
        Clear();
        Email.text = null;
        Password.text = null;
        Password1.text = null;
    }

    void Clear()
    {
        Status.text = null;
    }

    private void Awake()
    {
        RegisterButton.onClick.AddListener(() => { OnRegister(); });
        Back.onClick.AddListener(() => { gameObject.SetActive(false); LoginPage.SetActive(true); });

        Email.onValueChanged.AddListener((s) => Clear());
        Password.onValueChanged.AddListener((s) => Clear());
        Password1.onValueChanged.AddListener((s) => Clear());
    }

    private async void OnRegister()
    {
        Clear();
        Back.interactable = false;
        RegisterButton.interactable = false;

        string email = Email.text;
        string password = Password.text;
        string password1 = Password1.text;

        if(Login.CheckValidEmail(email))
        {
            if(password == password1)
            {
                if(password.Length >= 8)
                {
                    using(MemoryStream stream = new MemoryStream())
                    {
                        ByteProxy.Serialize(stream, 2);
                        StringProxy.Serialize(stream, email);
                        StringProxy.Serialize(stream, password);

                        byte[] outData = await TcpSender.SendMessage(stream.ToArray());

                        if(outData != null && outData.Length > 0)
                        {
                            switch(outData[0])
                            {
                                case 1:
                                    Status.text = "Registered Successfully";
                                    Status.color = Color.black;
                                    break;
                                case 2:
                                    Status.text = "Internal Server Error";
                                    Status.color = Color.red;
                                    break;
                                case 3:
                                    Status.text = "Email already exists";
                                    Status.color = Color.red;
                                    break;
                                case 254:
                                    Status.text = "Invalid Auth ID";
                                    Status.color = Color.red;
                                    break;
                                case 255:
                                    Status.text = "Internal Server Error";
                                    Status.color = Color.red;
                                    break;
                            }
                        }
                        else
                        {
                            Status.text = "No Internet";
                            Status.color = Color.red;
                        }
                    }
                }
                else
                {
                    Status.text = "Password length should be more than 8";                   
                    Status.color = Color.red;
                }
            }
            else
            {
                Status.text = "Password not matching";
                Status.color = Color.red;
            }
        }
        else
        {
            Status.text = "Invalid Email";
            Status.color = Color.red;
        }

        Back.interactable = true;
        RegisterButton.interactable = true;
    }
}
