﻿using UnityEngine;
using UnityEngine.UI;

class DeviceIDPage : MonoBehaviour
{
    [SerializeField] GameObject LoginPage;
    [SerializeField] GameObject ResultPage;
    [SerializeField] GameObject ChangePasswordPage;
    [SerializeField] Button SearchButton;
    [SerializeField] Button LogoutButton;
    [SerializeField] Button ChangePassword;

    private void Awake()
    {
        SearchButton.onClick.AddListener(() => { ResultPage.SetActive(true); gameObject.SetActive(false); });
        LogoutButton.onClick.AddListener(() => { LoginPage.SetActive(true); gameObject.SetActive(false); });
        ChangePassword.onClick.AddListener(() => { ChangePasswordPage.SetActive(true); gameObject.SetActive(false); });
    }
}
