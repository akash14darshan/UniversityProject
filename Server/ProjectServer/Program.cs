﻿using ProjectServer.Database;
using System;

namespace ProjectServer
{
    class Program
    {
        static void Main(string[] args)
        {
            UserManager.Init();
            new System.Threading.Thread(() => { TcpServer.Init(); }).Start();
            new System.Threading.Thread(() => { MatlabTCP.Init(); }).Start();
            while(true)
            {
                Console.Read();
            }
        }
    }
}
