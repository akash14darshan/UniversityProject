﻿using MongoDB.Driver;
using ProjectServer.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectServer.Database
{
    public static class UserManager
    {
        private static MongoDatabase<UserDocument> sm_database;

        public static void Init()
        {
            sm_database = new MongoDatabase<UserDocument>("Users");
        }

        public static async Task<UserDocument> GetUser(string email)
        {
            string email_l = email.ToLower();
            return await (await sm_database.Collection.FindAsync(Builders<UserDocument>.Filter.Eq(f => f.Email, email_l))).FirstOrDefaultAsync();
        }

        public static async Task<bool> SaveDocument(UserDocument doc)
        {
            try
            {
                if (doc != null)
                {
                    await sm_database.Collection.ReplaceOneAsync(f => f.Email == doc.Email, doc);
                    return true;
                }
            }
            catch(Exception)
            {
            }
            return false;
        }

        public static async Task<bool> CreateUser(string email, string password)
        {
            try
            {
                string email_l = email.ToLower();
                UserDocument doc = new UserDocument
                {
                    Email = email,
                    Password = password
                };
                await sm_database.Collection.InsertOneAsync(doc);
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
