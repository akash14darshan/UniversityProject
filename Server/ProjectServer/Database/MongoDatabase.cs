﻿using MongoDB.Driver;

namespace ProjectServer.Database
{
    public class MongoDatabase<T> where T : MongoDocument
    {
        private readonly IMongoClient m_client;
        private readonly IMongoDatabase m_database;

        public IMongoCollection<T> Collection { get; }

        public MongoDatabase(string collection)
        {
            string host = "163.172.255.37";
            string username = "Administrator";
            string password = "retardmf";
            string authDatabase = "admin";
            string iotDatabase = "iot";

            m_client = new MongoClient(new MongoClientSettings
            {
                Servers = new[] { new MongoServerAddress(host) },
                Credential = MongoCredential.CreateCredential(authDatabase, username, password),
                MinConnectionPoolSize = 10,
                MaxConnectionPoolSize = 1000
            });
            m_database = m_client.GetDatabase(iotDatabase);
            Collection = m_database.GetCollection<T>(collection);
            Collection.FindSync(f => false); // Initialize connections
        }
    }
}
