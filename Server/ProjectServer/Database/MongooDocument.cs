﻿using MongoDB.Bson;

namespace ProjectServer.Database
{
    public abstract class MongoDocument
    {
        public ObjectId Id { get; set; }
    }
}
