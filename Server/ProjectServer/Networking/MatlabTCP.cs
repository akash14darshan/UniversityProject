﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public static class MatlabTCP
{
    const int PORT_NO = 9001;

    static TcpListener Listener;

    public static void Init()
    {
        Listener = new TcpListener(IPAddress.Any, PORT_NO);
        Console.WriteLine("Started Matlab Server");
        Listener.Start();

        while (true)
        {
            try
            {
                TcpClient client = Listener.AcceptTcpClient();

                NetworkStream nwStream = client.GetStream();
                byte[] buffer = new byte[client.ReceiveBufferSize];

                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                HandleIncomingData(buffer, bytesRead);

                client.Close();
            }
            catch (Exception e)
            { 
                Console.WriteLine(e.ToString());
            }
        }
    }

    private static void HandleIncomingData(byte[] data, int length)
    {
        string msg = Encoding.ASCII.GetString(data,0 , length -1);
        string[] record = msg.Split(';');
        Console.WriteLine($"\nReceived new data at {DateTime.UtcNow.AddHours(5).AddMinutes(30)}");
        for(int i=0; i< record.Length; i++)
        {
            if(i == 0)
            {
                Console.WriteLine($"Email -> {record[0]}");
            }
            else
            {
                Console.WriteLine($"Record {i} -> {record[i]}");
            }
        }
        Console.WriteLine("End of data");
    }

    public static void Stop()
    {
        Listener.Stop();
    }
}
