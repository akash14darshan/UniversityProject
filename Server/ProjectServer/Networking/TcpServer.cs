﻿using ProjectServer.Serialization;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

public static class TcpServer
{
    const int PORT_NO = 9000;

    static TcpListener Listener;

    public static void Init()
    {
        Listener = new TcpListener(IPAddress.Any, PORT_NO);
        Console.WriteLine("Started IOT Server");
        Listener.Start();

        while(true)
        {
            try
            {
                TcpClient client = Listener.AcceptTcpClient();

                NetworkStream nwStream = client.GetStream();
                byte[] buffer = new byte[client.ReceiveBufferSize];

                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                byte[] returnData = HandleIncomingData(buffer, bytesRead);

                nwStream.Write(returnData, 0, returnData.Length);
                client.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }

    private static byte[] HandleIncomingData(byte[] data, int length)
    {
        try
        {
            using (MemoryStream stream = new MemoryStream(data, 0, length))
            {
                using(MemoryStream outgoing = new MemoryStream())
                {
                    byte opCode = ByteProxy.Deserialize(stream);

                    switch (opCode)
                    {
                        case 1: //login
                            string email1 = StringProxy.Deserialize(stream);
                            string password1 = StringProxy.Deserialize(stream);
                            byte result1 = ProjectServer.Networking.AuthenticationManager.OnLogin(email1, password1).GetAwaiter().GetResult();
                            ByteProxy.Serialize(outgoing, result1);
                            break;
                        case 2: //create acc
                            string email2 = StringProxy.Deserialize(stream);
                            string password2 = StringProxy.Deserialize(stream);
                            byte result2 = ProjectServer.Networking.AuthenticationManager.OnRegistration(email2, password2).GetAwaiter().GetResult();
                            ByteProxy.Serialize(outgoing, result2);
                            break;
                        case 3: //change pass
                            string email3 = StringProxy.Deserialize(stream);
                            string password = StringProxy.Deserialize(stream);
                            byte result3 = ProjectServer.Networking.AuthenticationManager.ChangePassword(email3, password).GetAwaiter().GetResult();
                            ByteProxy.Serialize(outgoing, result3);
                            break;
                        default:
                            return new byte[1] { 254 };
                    }

                    return outgoing.ToArray();
                }
            }
        }
        catch(Exception e)
        {
            Console.WriteLine(e.ToString());
            return new byte[1] { 255 };
        }
    }

    public static void Stop()
    {
        Listener.Stop();
    }
}