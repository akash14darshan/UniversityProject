﻿using ProjectServer.Database;
using ProjectServer.Views;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectServer.Networking
{
    public static class AuthenticationManager
    {
        public static async Task<byte> OnLogin(string email, string password)
        {
            UserDocument doc = await UserManager.GetUser(email);
            if (doc == null) return 3; //email doesnt exist

            if(doc.Password == password) //success
            {
                return 1;
            }
            else
            {
                return 2; //password doesnt match
            }
        }

        public static async Task<byte> OnRegistration(string email, string password)
        {
            UserDocument doc = await UserManager.GetUser(email);
            if (doc != null) return 3; //email already exists

            bool result = await UserManager.CreateUser(email, password);
            if (result) return 1; //success

            else return 2; //internal server error
        }

        public static async Task<byte> ChangePassword(string email, string newPass)
        {
            if(!string.IsNullOrEmpty(newPass))
            {
                UserDocument doc = await UserManager.GetUser(email);
                if (doc != null)
                {
                    if(doc.Password != newPass)
                    {
                        doc.Password = newPass;
                        bool res = await UserManager.SaveDocument(doc);
                        if (res) return 1;
                        else return 2;
                    }
                    else
                    {
                        return 3; //new pass same as old
                    }
                }
                else
                {
                    return 2; //did not change
                }
            }
            else
            {
                return 2; //did not change
            }
        }

        public static async Task<List<SimulationResult>> GetResult()
        {
            throw new System.NotImplementedException();
        }
    }
}
