﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProjectServer.Serialization
{
	public static class ListProxy<T>
	{
		public static void Serialize(Stream bytes, ICollection<T> instance, Action<Stream, T> serialization)
		{
			if (instance == null)
			{
				UInt16Proxy.Serialize(bytes, 0);
				return;
			}
			UShortProxy.Serialize(bytes, (ushort)instance.Count);
			foreach (T instance2 in instance)
			{
				serialization(bytes, instance2);
			}
		}

		public static List<T> Deserialize(Stream bytes, Func<Stream, T> serialization)
		{
			ushort num = UShortProxy.Deserialize(bytes);
			if (num == 0) { return new List<T>(); }
			List<T> list = new List<T>(num);
			for (int i = 0; i < (int)num; i++)
			{
				list.Add(serialization(bytes));
			}
			return list;
		}
	}
}
