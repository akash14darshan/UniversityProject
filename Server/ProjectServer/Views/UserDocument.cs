﻿using ProjectServer.Database;
using System.Collections.Generic;

namespace ProjectServer.Views
{
    public class UserDocument : MongoDocument
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public Dictionary<string, List<SimulationResult>> StoredResults { get; set; }
    }
}
