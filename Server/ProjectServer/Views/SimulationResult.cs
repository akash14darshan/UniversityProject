﻿using ProjectServer.Serialization;
using System;
using System.IO;

namespace ProjectServer.Views
{
    public class SimulationResult
    {
        public string Data1 { get; set; }
        public string Data2 { get; set; }
        public string Data3 { get; set; }
        public DateTime TimeStamp { get; set; }

        public static void Serialize(Stream stream, SimulationResult view)
        {
            int num = 0;
            using(var bytes = new MemoryStream())
            {
                if(!string.IsNullOrEmpty(view.Data1))
                {
                    StringProxy.Serialize(bytes, view.Data1);
                }
                else
                {
                    num |= 1;
                }

                if(!string.IsNullOrEmpty(view.Data2))
                {
                    StringProxy.Serialize(bytes, view.Data2);
                }
                else
                {
                    num |= 2;
                }

                if(!string.IsNullOrEmpty(view.Data3))
                {
                    StringProxy.Serialize(bytes, view.Data3);
                }
                else
                {
                    num |= 4;
                }

                DateTimeProxy.Serialize(bytes, view.TimeStamp);
                Int32Proxy.Serialize(stream, ~num);
                bytes.WriteTo(stream);
            }
        }

        public static void Deserialize(Stream stream)
        {
            int num = Int32Proxy.Deserialize(stream);
            SimulationResult result = new SimulationResult();
            if((num&1) != 0)
            {
                result.Data1 = StringProxy.Deserialize(stream);
            }
            if((num&2) != 0)
            {
                result.Data2 = StringProxy.Deserialize(stream);
            }
            if((num&4) != 0)
            {
                result.Data3 = StringProxy.Deserialize(stream);
            }
            result.TimeStamp = DateTimeProxy.Deserialize(stream);
        }
    }
}
